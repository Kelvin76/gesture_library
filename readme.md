# Gesture Library/Recognizer:

---
## Goal of this project
- Create a user-friendly android app
- Learning the basics of android development
- Trying to maximize the scalability and robustness of an android app

## Project requirements:
-  gradle version "6.6"
-  openjdk version "11.0.9.1"
-  Android SDK Version: API 30

## Run the application

This is an android (gradle) project. Hence, you can just install `./Gesture_Library.apk` in your emulator with `adb` and run it.
Or you can open this project in an IDE that supports building and running android application in an emulator. 

---
# Introduction of this application

Basically, this application allows the user to create, apply, and manage one-stroke touch gestures.
The following are descriptions of the 3 main interfaces the user will need to touch in this application.  

## Recognition
This interface allows the user to draw a one-stroke gesture.  
- Once the user drew a gesture, the 3 most similar gestures will be shown on the bottom (if exists at least 3 gestures in the library). Each gesture will be displayed as its name plus the thumbnail of it.
- The most confident guess will be marked with a cyan background.
- The "clear" button will clear the canvas.

## Library
Library interface will display all the gestures that is stored in this application.  
The user can edit and remove any gesture they want in the librarby.  
   - Thumbnail: each entry in the library will have a thumbnail of the gesture.
   - Edit (with a pencil symbol): when user press the edit button, a window for the user to redraw the gesture will popup. The name of the gesture will remain unchanged.
   - Remove (with a cancel symbol): the user can delete the gesture by pressing this button.

## Addition
The user can add gestures in this interface.  
There are two buttons at the bottom:  
- OK: the user can press this button to save the gesture.
   - When this button is clicked, the user will see a popup asking for the name of the gesture. If a gesture with the same name already exists, another popup will ask the user if they wish to overwrite the original gesture.
- Clear: the user can press this button to clear the canvas.

## Other details
- The library will be stored on the mobile.
- The drawing area only allows for one stroke. Hence, the last stroke will be removed every time the user tries to draw two strokes at once.
- The fragment for recognition and addition will display a text of "Please draw here." for the users if they just switched to these two views. This is intended for a user-friendly experience.

## Source for resources:
Four pngs resided in `/app/src/main/res/drawable`:
- ic_pencil.xml: https://www.flaticon.com/authors/prosymbols
- ic_delete.xml: https://www.flaticon.com/authors/pixel-perfect
