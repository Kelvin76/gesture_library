package Gesture.Main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.*;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.TextView;

// Disclaimer: This class is developed based on the sample code provided by Jeff Avery for cs349

@SuppressLint("AppCompatCustomView")
public class DrawingView extends ImageView {

    // drawing
    CustomPath path = null;
    Paint paintbrush = new Paint(Paint.ANTI_ALIAS_FLAG);
    Bitmap background;

    // special items for Recognization
    boolean hasObserver = false;
    HomeFragment observer;
    boolean hasText = false;
    TextView textView;

    // constructor
    public DrawingView(Context context) {
        super(context);
        paintbrush.setStyle(Paint.Style.STROKE);
        paintbrush.setStrokeWidth(8);
    }

    // constructor
    public DrawingView(Context context, HomeFragment observer) {
        this(context);
        this.hasObserver = true;
        this.observer = observer;
    }

    // constructor
    public DrawingView(Context context, TextView textView) {
        this(context);
        this.hasText = true;
        this.textView = textView;
    }
    
    // constructor
    public DrawingView(Context context, HomeFragment observer, TextView textView) {
        this(context,observer);
        this.hasText = true;
        this.textView = textView;
    }

    // we save a lot of points because they need to be processed
    // during touch events e.g. ACTION_MOVE
    float x1, x2, y1, y2, old_x1, old_y1, old_x2, old_y2;
    float mid_x = -1f, mid_y = -1f, old_mid_x = -1f, old_mid_y = -1f;
    int p1_id, p1_index, p2_id, p2_index;

    // store cumulative transformations
    // the inverse matrix is used to align points with the transformations - see below
    Matrix matrix = new Matrix();
    Matrix inverse;

    // capture touch events (down/move/up) to create a path/stroke that we draw later
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getPointerCount()) {
            // 1 point is drawing or erasing
            case 1:
                p1_id = event.getPointerId(0);
                p1_index = event.findPointerIndex(p1_id);

                // invert using the current matrix to account for pan/scale
                // inverts in-place and returns boolean
                inverse = new Matrix();
                matrix.invert(inverse);

                // mapPoints returns values in-place
                float[] inverted = new float[] { event.getX(p1_index), event.getY(p1_index) };
                inverse.mapPoints(inverted);
                x1 = inverted[0];
                y1 = inverted[1];

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        if(hasText){
                            textView.setVisibility(GONE);
                            hasText=false;
                        }

                        //System.out.println("[Drawing View] Action down.");
                        // clear the canvas first
                        clearCanvas();

                        path = new CustomPath();
                        path.moveTo(x1, y1);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        //System.out.println("[Drawing View] Action move.");
                        path.lineTo(x1, y1);
                        break;
                    case MotionEvent.ACTION_UP:
                        // if observer exists, notify it
                        if(hasObserver){
                            observer.notifyIt();
                        }
                        //System.out.println("[Drawing View] Action up.");
                        break;
                }
                break;
            // 2 points is zoom/pan
//            case 2:
//                // point 1
//                p1_id = event.getPointerId(0);
//                p1_index = event.findPointerIndex(p1_id);
//
//                // mapPoints returns values in-place
//                inverted = new float[] { event.getX(p1_index), event.getY(p1_index) };
//                inverse.mapPoints(inverted);
//
//                // first pass, initialize the old == current value
//                if (old_x1 < 0 || old_y1 < 0) {
//                    old_x1 = x1 = inverted[0];
//                    old_y1 = y1 = inverted[1];
//                } else {
//                    old_x1 = x1;
//                    old_y1 = y1;
//                    x1 = inverted[0];
//                    y1 = inverted[1];
//                }
//
//                // point 2
//                p2_id = event.getPointerId(1);
//                p2_index = event.findPointerIndex(p2_id);
//
//                // mapPoints returns values in-place
//                inverted = new float[] { event.getX(p2_index), event.getY(p2_index) };
//                inverse.mapPoints(inverted);
//
//                // first pass, initialize the old == current value
//                if (old_x2 < 0 || old_y2 < 0) {
//                    old_x2 = x2 = inverted[0];
//                    old_y2 = y2 = inverted[1];
//                } else {
//                    old_x2 = x2;
//                    old_y2 = y2;
//                    x2 = inverted[0];
//                    y2 = inverted[1];
//                }
//
//                // midpoint
//                mid_x = (x1 + x2) / 2;
//                mid_y = (y1 + y2) / 2;
//                old_mid_x = (old_x1 + old_x2) / 2;
//                old_mid_y = (old_y1 + old_y2) / 2;
//
//                // distance
//                float d_old = (float) Math.sqrt(Math.pow((old_x1 - old_x2), 2) + Math.pow((old_y1 - old_y2), 2));
//                float d = (float) Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
//
//                // pan and zoom during MOVE event
//                if (event.getAction() == MotionEvent.ACTION_MOVE) {
//                    System.out.println("[Drawing View] Multitouch move.");
//                    // pan == translate of midpoint
//                    float dx = mid_x - old_mid_x;
//                    float dy = mid_y - old_mid_y;
//                    matrix.preTranslate(dx, dy);
//
//                    System.out.println("[Drawing View] translate: " + dx + "," + dy);
//
//                    // zoom == change of spread between p1 and p2
//                    float scale = d/d_old;
//                    scale = Math.max(0, scale);
//                    matrix.preScale(scale, scale, mid_x, mid_y);
//
//                    System.out.println("[Drawing View] translate: " + "scale: " + scale);
//
//                    // reset on up
//                } else if (event.getAction() == MotionEvent.ACTION_UP) {
//                    old_x1 = -1f;
//                    old_y1 = -1f;
//                    old_x2 = -1f;
//                    old_y2 = -1f;
//                    old_mid_x = -1f;
//                    old_mid_y = -1f;
//                }
//                break;
            default:
                break;
        }
        return true;
    }


    // accessors
    public Bitmap getBitmap(){
            return background;
    }

    // mutators
    // set image as background
    public void setImage(Bitmap bitmap) {
        this.background = bitmap;
    }

    public void setPath(CustomPath path) {
        this.path = path;
    }

    public void drawCanvas(Canvas canvas){
        // draw lines over it
        canvas.drawPath(path, paintbrush);
        // System.out.println("Finished drawing pathes");
    }

    public CustomPath getPath(){
        return path;
    }

    public void clearCanvas(){
        matrix.reset();
        path = null;

        Canvas canvas = new Canvas(background);
        // draw lines over it
        if(path != null){
            canvas.drawPath(path, paintbrush);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // apply transformations from the event handler above
        canvas.concat(matrix);

        // draw background
        if (background != null) {
            this.setImageBitmap(background);
        }

        // draw lines over it
        if(path != null){
            canvas.drawPath(path, paintbrush);
        }
    }
}