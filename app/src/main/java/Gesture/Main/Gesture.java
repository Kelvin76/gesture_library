package Gesture.Main;

import android.graphics.*;

import java.io.Serializable;
import java.util.ArrayList;


/*
TODO


 */


public class Gesture implements Serializable {
    private String name; // name of this gesture
    private CustomPath iconPath; // icon used to display
    private ArrayList<float[]> matchPoints; // the list of points that we can use to compare

    // some constants
    private final int n = 256; // number of points we store in matchPoints
    private final float standardX = 190;
    private final float standardY = 350;

    Gesture(String name, DrawingView input){
        this.name = name;
        this.iconPath = input.getPath();

        if(iconPath == null){
            System.out.println("Path is null when initializing");
        }

        transform(input);
    }

    // only used for comparison
    Gesture(DrawingView input){
        transform(input);
    }


    // transform the images into a uniform format
    public void transform(DrawingView image){
        matchPoints = new ArrayList<>();

        float degrees=0;
        //float aCoordinates[] = {0f, 0f};
        float[] startPt = {0f,0f},middlePt = {0f,0f};

        PathMeasure pm;

        // declare several temp variables
        CustomPath mPath = new CustomPath(image.getPath());
        // matrix is used to translate, rotate, and scale
        Matrix mMatrix = new Matrix();
        // bounds is used to calculate the center
        RectF bounds = new RectF();
        mPath.computeBounds(bounds, true);

        // translate: move the center of the path to 0,0
        mMatrix.postTranslate(-bounds.centerX(),-bounds.centerY());
        mPath.computeBounds(bounds, true);

        // scale: scale the path,
        mMatrix.postScale(standardX/bounds.width(),standardY/bounds.height(),0,0);

        // rotate: rotate the path with such a difference in the degrees
        // calculate the difference of the angle between the line from staring pt. to the middle pt. and 0 degree
        pm = new PathMeasure(mPath,false);
        float totalLength = pm.getLength(), distance = 0f, speed = totalLength / n;

        // calculate the degrees
        // get the middle point
        pm.getPosTan(totalLength * 0, startPt, null);
        pm.getPosTan(totalLength * 0.5f, middlePt, null);
        degrees = (float)(Math.atan2(middlePt[1]-startPt[1],startPt[0]-middlePt[0]) * 180 / Math.PI);

        mMatrix.postRotate(degrees,0,0);

        // finally transform the path with the given matrix
        mPath.transform(mMatrix);

        // get the list of 128 points and store them in matchPoints
        for(int index = 0; index < n; ++index){
            // get point from the path
            float[] aCoordinates = new float[2];
            pm.getPosTan(distance, aCoordinates, null);
            matchPoints.add(aCoordinates);
            distance += speed;
        }
    }

    // class functions

    // compare two gestures, the smaller the output, the similar they are
    public double compare(Gesture otherGesture){
        double averageOff = 0;
        ArrayList<float[]> otherPoints = otherGesture.getMatchPoints();

        for(int i = 0;i < n; ++i){
            averageOff += Math.sqrt(Math.pow(matchPoints.get(i)[0]-otherPoints.get(i)[0],2)+Math.pow(matchPoints.get(i)[1]-otherPoints.get(i)[1],2))/n;
        }
        return averageOff;
    }

    // the id of a Gesture is its name. Name is unique!
    public String toString(){
        return "gestrue{" +
                "name='" + name + '\''+
                '}';
    }

    // for comparison
    public boolean equals(Object o){
        if(!(o instanceof Gesture)){
            return false;
        }
        if(this == o || this.name == ((Gesture)o).name){
            return true;
        }
        return false;
    }

    // accessors
    public String getName(){
        return name;
    }
    public ArrayList<float[]> getMatchPoints() {
        return matchPoints;
    }
    public CustomPath getIconPath(){
        // if(iconPath == null){
           // System.out.println("[Exception] path should not be null");
        // }
        return iconPath;
    }

    // mutators
    public void setName(String name) {
        this.name = name;
    }
    public void setIconPath(CustomPath iconPath) {
        this.iconPath = iconPath;
    }
    public void setmatchPath(ArrayList<float[]> matchPoints) {
        this.matchPoints = matchPoints;
    }
}