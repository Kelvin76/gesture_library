package Gesture.Main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import java.util.ArrayList;
import java.util.List;

public class LibraryFragment extends Fragment {

    private SharedViewModel mViewModel;
    private View root;

    // the display info of all gestures
    private ListView gesture_list;
    private GestureAdapter adapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        // register the model and root view
        mViewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);
        root = inflater.inflate(R.layout.fragment_library, container, false);
        mViewModel.setView(root);
        mViewModel.setContext(this.getContext());

        // get the gestures
        ArrayList<Gesture> gestures = mViewModel.getLibrary();

        if(!gestures.isEmpty()){
            // initialize adapter
            adapter = new GestureAdapter(gestures,mViewModel);
            gesture_list = root.findViewById(R.id.LIB_MAIN_LIST);
            gesture_list.setAdapter(adapter);
        }

        return root;
    }






    // Adapter class
    class GestureAdapter extends BaseAdapter {

        private final SharedViewModel mViewModel;

        private final List<Gesture> gestureList;

        public GestureAdapter(List<Gesture> gestureList, SharedViewModel mViewModel){
            this.gestureList = gestureList;
            this.mViewModel = mViewModel;
        }

        @Override
        public int getCount() {
            return gestureList.size();
        }

        @Override
        public Object getItem(int position) {
            return gestureList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        // customize the way of parsing
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {

                holder=new ViewHolder();

                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.gesture_item, parent,false);
                //convertView = m_Inflater.inflate(R.layout.gesture_item, null);

                holder.icon = convertView.findViewById(R.id.LIB_gesture_icon);
                holder.name = convertView.findViewById(R.id.LIB_gesture_name);
                holder.editBtn = convertView.findViewById(R.id.LIB_BTN_EDIT);
                holder.delBtn = convertView.findViewById(R.id.LIB_BTN_DEL);

                convertView.setTag(holder);
            }else {
                holder = (ViewHolder)convertView.getTag();
            }

            // get the current gesture instance
            Gesture gesture = gestureList.get(position);

            if(gesture.getIconPath()!=null){
                // draw diagram for the icon
                // prepare the canvas to draw
                Bitmap bitmap = Bitmap.createBitmap(150, 200, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                paint.setColor(Color.BLACK);
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeWidth(30);

                // scale the canvas then draw
                canvas.scale(0.125f,0.117f);
                canvas.drawPath(gesture.getIconPath(),paint);

                // set the image
                holder.icon.setImageBitmap(bitmap);
            }

            holder.name.setText(gesture.getName());
            holder.editBtn.setImageDrawable(parent.getContext().getDrawable(R.drawable.ic_pencil));
            holder.delBtn.setImageDrawable(parent.getContext().getDrawable(R.drawable.ic_remove));

            // add listeners
            holder.editBtn.setOnClickListener(new EditListener(position));
            holder.delBtn.setOnClickListener(new DeleteListener(position));

            return convertView;
        }

        class EditListener implements View.OnClickListener {
            int mPosition;
            public EditListener(int position){mPosition = position;}
            @Override
            public void onClick(View v) {
                final Gesture ges = (Gesture)getItem(mPosition);

                // popup the view of addition
                final AlertDialog.Builder builder = new AlertDialog.Builder(root.getContext())
                        .setTitle("Replace the gesture \""+ges.getName()+"\"")
                        .setIcon(android.R.drawable.ic_input_add);

                // draw diagram for the icon
                // prepare the canvas to draw
                DrawingView pageView = new DrawingView(root.getContext());

                // setup the canvas for drawing
                pageView.setMinimumWidth(1000);
                pageView.setMinimumHeight(1700);

                // setup background
                Bitmap bitmap = Bitmap.createBitmap(1000, 1700, Bitmap.Config.ARGB_8888);
                pageView.setImage(bitmap);

                // set the original path
                pageView.setPath(ges.getIconPath());

                // add this view
                builder.setView(pageView);

                builder.setPositiveButton("OK",new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // save the new gesture
                        mViewModel.replaceGesture(pageView,ges.getName());

                        System.out.println("Saved a new gesture: "+ges.getName()+".");

                        adapter.notifyDataSetChanged();

                        dialog.cancel();
                    }
                })
                .setNegativeButton("Cancel",new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.cancel();
                    }
                })
                .show();

                // deprecated, but might be useful
                // the following are logic for change the name of the gesture
                /*
                // pop window to rename the gesture
                final AlertDialog.Builder builder = new AlertDialog.Builder(root.getContext())
                        .setTitle("Input a new name:")
                        .setIcon(android.R.drawable.ic_input_add);

                // set the edit text for the dialog box
                final EditText et_Name = new EditText(root.getContext());
                builder.setView(et_Name);

                // set the buttons for the dialog box
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String gesture_Name = et_Name.getText().toString().trim();

                        // if the name exists, ask the user to choose another name
                        if(mViewModel.existsName(gesture_Name)){
                            new AlertDialog.Builder(root.getContext())
                                    .setTitle("This name already exists. Please enter another one.")
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setPositiveButton("OK", null)
                                    .show();
                        }else{
                            // update the database
                            mViewModel.changeName(ges.getName(),gesture_Name);

                            //set the new name for the gesture
                            ges.setName(gesture_Name);

                            adapter.notifyDataSetChanged();

                            dialog.cancel();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .show();
                 */
            }
        }

        class DeleteListener implements View.OnClickListener {
            int mPosition;
            public DeleteListener(int position){mPosition = position;}
            @Override
            public void onClick(View v) {
                Gesture ges = (Gesture)getItem(mPosition);

                //System.out.println("Deleted a gesture, gesture name:"+ges.getName()+".");
                // delete the gesture
                mViewModel.deleteGesture(ges.getName());

                adapter.notifyDataSetChanged();
            }
        }

        // for extracting
        final class ViewHolder {
            public ImageView icon;
            public TextView name;
            public ImageButton editBtn;
            public ImageButton delBtn;
        }
    }
}