package Gesture.Main;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import androidx.lifecycle.ViewModel;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;

public class SharedViewModel extends ViewModel implements Serializable{

    private Context context;
    private DATA gestures;

    private ArrayList<Gesture> gestureList = new ArrayList<>();

    // some constant
    private String fileName = "gesture_file_save";

    @SuppressLint("StaticFieldLeak")
    private View root;

    public SharedViewModel() {
        gestures = new DATA();
    }

    // add the gesture, used for addition
    public void addGesture(final DrawingView imageV, final String name){

        // if name already exists
        if(gestures.containsKey(name)){

            // pop alert for overwrite
            // ask for a name from the dialog box
            new AlertDialog.Builder(root.getContext())
                .setTitle("Gesture of the same name exists! Press sure to overwrite.")
                .setIcon(android.R.drawable.ic_notification_overlay)
                // set the buttons for the dialog box
                .setPositiveButton("Sure", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();

                        // user press yes to overwrite the original gesture
                        saveGesture(name, new Gesture(name,imageV));

                        //clear the canvas after the image is saved
                        imageV.clearCanvas();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .show();

        }else{
            // save the gesture directly
            saveGesture(name, new Gesture(name,imageV));

            //clear the canvas after the image is saved
            imageV.clearCanvas();
        }
    }

    // assume the gesture exists
    public void replaceGesture(final DrawingView imageV, final String name){
        Gesture newGes = new Gesture(name,imageV);

        // update the library list
        if(!gestureList.isEmpty()){
            for(int i = 0 ; i <gestureList.size();++i){
                if(gestureList.get(i) == gestures.get(name)){
                    gestureList.set(i,newGes);
                    break;
                }
            }
        }

        // save the gesture in the database
        saveGesture(name, newGes);
    }


    // get the information about the saved gestures, used for library
    public ArrayList<Gesture> getLibrary(){
        gestureList.clear();
        gestureList.addAll(gestures.values());
        gestureList.sort(new Comparator<Gesture>() {
            @Override
            public int compare(Gesture o1, Gesture o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        return gestureList;
    }

    // MVC functions
    public void setView(View view){
        root=view;
    }

    public void deleteGesture(String name){
        if(!gestureList.isEmpty()){
            gestureList.remove(gestures.get(name));
        }
        removeGesture(name);
    }

    public boolean existsName(String name){
        return (gestures.containsKey(name));
    }

    public void changeName(String name, String newName){
        // add and remove
        saveGesture(newName,Objects.requireNonNull(gestures.get(name)));
        removeGesture(name);
    }

    public void setContext(Context context){
        this.context = context;
    }

    public void saveGesture(String name, Gesture ges){
        gestures.put(name,ges);
        // save into the file
        try{
            ObjectOutputStream oos = new ObjectOutputStream(context.openFileOutput(fileName,android.content.Context.MODE_PRIVATE));

            // serialize
            // System.out.println("[SAVE] save add.");
            oos.writeObject(gestures);
            oos.close();

            //oos.writeObject(new data(gestures));


        }catch (Exception ex){
            System.out.println("[Exception] Save: "+ex.toString());
        }
    }

    public void removeGesture(String name){
        gestures.remove(name);
        // save into the file
        try{
            ObjectOutputStream oos = new ObjectOutputStream(context.openFileOutput(fileName,android.content.Context.MODE_PRIVATE));

            // serialize
            // System.out.println("[SAVE] remove add.");
            oos.writeObject(gestures);
            oos.close();

            //oos.writeObject(new data(gestures));
        }catch (Exception ex){
            System.out.println("[Exception] Remove: "+ex.toString());
        }
    }

    public void readData(){
        // load the file
        try{
            ObjectInputStream ois = new ObjectInputStream(context.openFileInput(fileName));
            DATA data = (DATA)ois.readObject();

            // deserialize
            // System.out.println("[LOAD] tries to load.");
            if(data != null){
                // System.out.println("[LOAD] loaded.");
                gestures = data;
            }

            ois.close();
        }catch (Exception ex){
            System.out.println("[Exception] Load: "+ex.toString());
        }
    }
}