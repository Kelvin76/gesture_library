package Gesture.Main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.*;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

public class AdditionFragment extends Fragment {

    private SharedViewModel mViewModel;
    private View root;

    private Button BTN_OK;
    private Button BTN_CLEAR;

    // canvas
    private DrawingView pageView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        // register the model and root view
        mViewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);
        root = inflater.inflate(R.layout.fragment_addition, container, false);
        mViewModel.setView(root);
        mViewModel.setContext(this.getContext());

        // setup the canvas for drawing
        pageView = new DrawingView(root.getContext(),root.findViewById(R.id.ADD_INSTRUCT_TEXT));
        pageView.setMinimumWidth(1000);
        pageView.setMinimumHeight(1700);

        // setup background
        Bitmap image = Bitmap.createBitmap(200, 300, Bitmap.Config.ARGB_8888);
        pageView.setImage(image);

        // add the canvas to layout
        LinearLayout layout = root.findViewById(R.id.ADD_CANVAS);
        layout.addView(pageView);
        layout.setEnabled(true);

        // buttons
        BTN_OK = root.findViewById(R.id.ADD_BTN_OK);
        BTN_CLEAR = root.findViewById(R.id.ADD_BTN_CLEAR);

        // register events for buttons
        BTN_OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // does not allow register of no path
                if(pageView.getPath() == null){
                    // ask for a name from the dialog box
                    new AlertDialog.Builder(root.getContext())
                            .setTitle("Please draw a gesture first.")
                            .setIcon(android.R.drawable.ic_notification_overlay)
                            .setPositiveButton("OK", null)
                            .show();
                }
                // register the name for it
                else{
                    // ask for a name from the dialog box
                    AlertDialog.Builder builder = new AlertDialog.Builder(root.getContext())
                            .setTitle("Input a name for this gesture:")
                            .setIcon(android.R.drawable.ic_input_add);

                    // set the edit text for the dialog box
                    final EditText et_Name = new EditText(root.getContext());
                    builder.setView(et_Name);

                    // set the buttons for the dialog box
                    builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String gesture_Name = et_Name.getText().toString().trim();

                            System.out.println("Saved a new gesture: "+gesture_Name+".");

                            //save the image to model
                            mViewModel.addGesture(pageView, gesture_Name);

                            dialog.cancel();
                        }
                    })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .show();
                }
            }
        });


        BTN_CLEAR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // clear the canvas
                pageView.clearCanvas();
            }
        });

        return root;
    }
}