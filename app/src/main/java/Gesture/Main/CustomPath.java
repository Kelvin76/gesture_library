package Gesture.Main;

import android.graphics.Path;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;

// implements serializable so the data can be stored on the mobile
public class CustomPath extends Path implements Serializable {
    // actions stores all the actions made onto this path
    private ArrayList<pAct> actions = new ArrayList<>();

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException{
        in.defaultReadObject();
        initializePath();
    }

    CustomPath(){}

    CustomPath(CustomPath path){
        super(path);
        this.actions.addAll(path.actions);
    }

    @Override
    public void moveTo(float x, float y) {
        actions.add(new PointAct(x, y));
        super.moveTo(x, y);
    }
    @Override
    public void lineTo(float x, float y){
        actions.add(new LineAct(x, y));
        super.lineTo(x, y);
    }

    // initialize the path based on the actions stored
    private void initializePath(){
        for(pAct p : actions){
            if(p.getType() == pAct.pActType.MOVE_TO){
                super.moveTo(p.getX(), p.getY());
            } else if(p.getType() == pAct.pActType.LINE_TO){
                super.lineTo(p.getX(), p.getY());
            }
        }
    }

    // pAct is the interface for path actions
    interface pAct {
        enum pActType {LINE_TO,MOVE_TO};
        pActType getType();
        float getX();
        float getY();
    }
    class PointAct implements pAct, Serializable{
        private final float x;
        private final float y;
        public PointAct(float x, float y){
            this.x = x;
            this.y = y;
        }
        @Override
        public pActType getType() {
            return pAct.pActType.MOVE_TO;
        }
        @Override
        public float getX() { return x; }
        @Override
        public float getY() { return y; }
    }
    class LineAct implements pAct, Serializable{
        private final float x;
        private final float y;
        public LineAct(float x, float y){
            this.x = x;
            this.y = y;
        }
        @Override
        public pActType getType() {
            return pAct.pActType.LINE_TO;
        }
        @Override
        public float getX() {
            return x;
        }
        @Override
        public float getY() {
            return y;
        }
    }
}