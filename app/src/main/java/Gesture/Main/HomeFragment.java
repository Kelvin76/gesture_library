package Gesture.Main;

import android.graphics.*;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import java.util.ArrayList;
import java.util.PriorityQueue;


public class HomeFragment extends Fragment {

    private SharedViewModel mViewModel;
    private View root;

    private Button BTN_CLEAR;

    // canvas
    private DrawingView pageView;

    // some GUI components
    private ImageView iView1;
    private ImageView iView2;
    private ImageView iView3;
    private TextView tView1;
    private TextView tView2;
    private TextView tView3;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        // register the model and root view
        mViewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);
        root = inflater.inflate(R.layout.fragment_home, container, false);
        mViewModel.setView(root);
        mViewModel.setContext(this.getContext());


        // Comment this before push
        // clean the file
//        try{
//            ObjectOutputStream oos = new ObjectOutputStream(root.getContext().openFileOutput("gesture_file_save",android.content.Context.MODE_PRIVATE));
//
//            // serialize
//            // System.out.println("[SAVE] save add.");
//            oos.writeObject(null);
//            oos.close();
//            //oos.writeObject(new data(gestures));
//        }catch (Exception ex){
//            System.out.println("[Exception] Save: "+ex.toString());
//        }


        mViewModel.readData();

        // setup the canvas for drawing
        pageView = new DrawingView(root.getContext(),this, root.findViewById(R.id.REC_INSTRUCT_TEXT));
        pageView.setMinimumWidth(1000);
        pageView.setMinimumHeight(1700);

        // setup background
        Bitmap image = Bitmap.createBitmap(200, 300, Bitmap.Config.ARGB_8888);
        pageView.setImage(image);

        // add the canvas to layout
        LinearLayout layout = root.findViewById(R.id.REC_CANVAS);
        layout.addView(pageView);
        layout.setEnabled(true);

        // buttons
        BTN_CLEAR = root.findViewById(R.id.REC_BTN_CLEAR);

        // register events for buttons
        BTN_CLEAR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //clear the canvas
                pageView.clearCanvas();

                iView1.setVisibility(View.INVISIBLE);
                tView1.setVisibility(View.INVISIBLE);

                iView2.setVisibility(View.INVISIBLE);
                tView2.setVisibility(View.INVISIBLE);

                iView3.setVisibility(View.INVISIBLE);
                tView3.setVisibility(View.INVISIBLE);
            }
        });

        // assign the corresponding imageViews and texts
        iView1 = root.findViewById(R.id.REC_MATCH_1);
        tView1 = root.findViewById(R.id.REC_MATCH_TEXT_1);

        iView2 = root.findViewById(R.id.REC_MATCH_2);
        tView2 = root.findViewById(R.id.REC_MATCH_TEXT_2);

        iView3 = root.findViewById(R.id.REC_MATCH_3);
        tView3 = root.findViewById(R.id.REC_MATCH_TEXT_3);

        return root;
    }

    // this is not MVC!! Straightly tied two objects.
    // got notified by the model
    public void notifyIt(){
        // the drawing is finished, we should find the 3 best matches
        double di;
        Gesture curGes = new Gesture(pageView);

        // TODO
        // optimize this to be a heap with only 3 elements

        // initialize a heap for 3 min number
        PriorityQueue<GesturePair> heap = new PriorityQueue<>(3);

        // get all the gestures
        ArrayList<Gesture> gestures = mViewModel.getLibrary();

        for(Gesture ges : gestures){
            di = curGes.compare(ges);
            // System.out.println("Off:"+di+".");
            heap.add(new GesturePair(ges,di));
        }

        // assign the three best matches
        GesturePair g1,g2,g3;
        g1 = heap.poll();
        g2 = heap.poll();
        g3 = heap.poll();

        // initialize the painter
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(30);

        // assign the views
        if(g1!=null){
            System.out.println("\n\r1st Match:"+g1.gesture.getName()+". Off:"+g1.di+".");
            // set the images

            // draw diagram for the icon
            // prepare the canvas to draw
            Bitmap bitmap = Bitmap.createBitmap(150, 170, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);

            // scale the canvas then draw
            canvas.scale(0.14f,0.08f);

            // draw a rectangle to show it's the best match
            paint.setColor(Color.CYAN);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawRect(new RectF(0,0,1200,1700),paint);

            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawPath(g1.gesture.getIconPath(),paint);

            iView1.setImageBitmap(bitmap);

            // set the text
            tView1.setText(g1.gesture.getName());

            // set visibility
            iView1.setVisibility(View.VISIBLE);
            tView1.setVisibility(View.VISIBLE);
        }else{
            // set visibility
            iView1.setVisibility(View.INVISIBLE);
            tView1.setVisibility(View.INVISIBLE);
        }

        // assign the views
        if(g2!=null){
            System.out.println("2nd Match:"+g2.gesture.getName()+". Off:"+g2.di+".");
            // set the images

            // draw diagram for the icon
            // prepare the canvas to draw
            Bitmap bitmap = Bitmap.createBitmap(150, 180, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);

            // scale the canvas then draw
            canvas.scale(0.14f,0.09f);
            canvas.drawPath(g2.gesture.getIconPath(),paint);

            iView2.setImageBitmap(bitmap);

            // set the text
            tView2.setText(g2.gesture.getName());

            // set visibility
            iView2.setVisibility(View.VISIBLE);
            tView2.setVisibility(View.VISIBLE);
        }else{
            // set visibility
            iView2.setVisibility(View.INVISIBLE);
            tView2.setVisibility(View.INVISIBLE);
        }

        // assign the views
        if(g3!=null){
            System.out.println("3rd Match:"+g3.gesture.getName()+". Off:"+g3.di+".");
            // set the images

            // draw diagram for the icon
            // prepare the canvas to draw
            Bitmap bitmap = Bitmap.createBitmap(150, 180, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);

            // scale the canvas then draw
            canvas.scale(0.14f,0.09f);
            canvas.drawPath(g3.gesture.getIconPath(),paint);

            iView3.setImageBitmap(bitmap);

            // set the text
            tView3.setText(g3.gesture.getName());

            // set visibility
            iView3.setVisibility(View.VISIBLE);
            tView3.setVisibility(View.VISIBLE);
        }else{
            // set visibility
            iView3.setVisibility(View.INVISIBLE);
            tView3.setVisibility(View.INVISIBLE);
        }






/*

        // for testing purpose
        ArrayList<float[]> points = new ArrayList<>();
        int n = 128;
        float standardSize = 400, scale,degrees=0;
        //float aCoordinates[] = {0f, 0f};
        float[] startPt = {0f,0f},middlePt = {0f,0f};

        PathMeasure pm;

        // declare several temp variables
        Path mPath = pageView.getPath();
        // matrix is used to translate, rotate, and scale
        Matrix mMatrix = new Matrix();
        // bounds is used to calculate the center
        RectF bounds = new RectF();
        mPath.computeBounds(bounds, true);

        // translate: move the center of the path to 0,0
        mMatrix.postTranslate(-bounds.centerX(),-bounds.centerY());
        mPath.computeBounds(bounds, true);

        // scale: scale the path,
        scale = standardSize / Math.max(bounds.height(),bounds.width());
        mMatrix.postScale(scale,scale,0,0);

        // rotate: rotate the path with such a difference in the degrees
        // calculate the difference of the angle between the line from staring pt. to the middle pt. and 0 degree
        pm = new PathMeasure(mPath,false);
        float totalLength = pm.getLength(), distance = 0f, speed = totalLength / 20;

        // calculate the degrees
        // get the middle point
        pm.getPosTan(totalLength * 0, startPt, null);
        pm.getPosTan(totalLength * 0.5f, middlePt, null);
        degrees = (float)(Math.atan2((double)(middlePt[1]-startPt[1]),(double)(startPt[0]-middlePt[0])) * 180 / Math.PI);

        mMatrix.postRotate(degrees,0,0);

        mMatrix.postTranslate(bounds.centerX(),bounds.centerY());

        // finally transform the path with the given matrix
        mPath.transform(mMatrix);*/

    }

    class GesturePair implements Comparable<GesturePair>{
        public Gesture gesture;
        public double di;

        GesturePair(Gesture gesture, double di){
            this.gesture = gesture;
            this.di = di;
        }

        @Override
        public int compareTo(GesturePair o) {
            if(this.di == o.di){
                return 0;
            }
            return (this.di < o.di)?-1:1;
        }

        @Override
        public String toString() {
            return "GesturePair{"+
                    "di:"+di+
                    "}";
        }
    }

}