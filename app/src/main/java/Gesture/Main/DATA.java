package Gesture.Main;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;

// used to store the data
class DATA implements Serializable {
    private HashMap<String,Gesture> gestures = new HashMap<>();

    DATA(){}
    public HashMap<String,Gesture> getGestures(){
        return gestures;
    }
    public void setGestures(HashMap<String,Gesture> gestures){
        this.gestures = gestures;
    }
    public void put(String name, Gesture ges){
        gestures.put(name,ges);
    }
    public void clear(){
        gestures.clear();
    }
    public void remove(String name){
        gestures.remove(name);
    }
    public Gesture get(String name){
        return gestures.get(name);
    }
    public boolean containsKey(String name){
        return gestures.containsKey(name);
    }
    public Collection<Gesture> values(){
        return gestures.values();
    }
}